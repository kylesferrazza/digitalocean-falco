# digitalocean-falco

This repo is for my take on the [DigitalOcean Kubernetes Challenge][challenge].

[challenge]: https://www.digitalocean.com/community/pages/kubernetes-challenge



## Prompt

> **Deploy a security and compliance system**
>
> When you deploy an image you may also deploy a security threat, and images can become quickly insecure as new threats are discovered and disclosed. To continuously monitor and detect threats, you can deploy a tool like Falco.


# Cluster Setup

Terraform makes setting up a kubernetes cluster on DigitalOcean dead simple and easily repeatable!

[`do_cluster.tf`](/terraform/do_cluster.tf) defines a cluster in the `nyc1` DigitalOcean region, with the latest (at time of writing) available k8s version:

```
name   = "falco-cluster"
region = "nyc1"
version = "1.21.5-do.0"
```

`do_cluster.tf` also specifies a 4-node cluster:

```
node_pool {
  name       = "worker-pool"
  size       = "s-2vcpu-2gb"
  node_count = 4
}
```

After runnning `terraform apply`, terraform uses the DigitalOcean API to spin up a 4-node Kubernetes cluster with no hassle!

![cluster in digitalocean web UI](/writeup/digitalocean-cluster.png)

# Deploying Helm Charts with Terraform
The [terraform Helm provider](https://registry.terraform.io/providers/hashicorp/helm/latest/docs) makes setting up Helm charts on the terraform-created kubernetes cluster very simple.

The Helm provider can be configured using the outputs from the DigitalOcean provider:

```
provider "helm" {
  kubernetes {
    host = digitalocean_kubernetes_cluster.falco_cluster.endpoint
    token = digitalocean_kubernetes_cluster.falco_cluster.kube_config[0].token
    cluster_ca_certificate = base64decode(
      digitalocean_kubernetes_cluster.falco_cluster.kube_config[0].cluster_ca_certificate
    )
  }
}
```

This way, after terraform is done provisioning a DigitalOcean Kubernetes cluster, the Helm provider is already configured and pointed at it, ready to install applications.

# Certificates
In order for Falco to be able to talk over gRPC to `falco-exporter`, we need to set up mutual TLS authentication.

This requires mutually trusted client and server TLS certificates. In order to generate these TLS certificates, I utilize the [TLS provider](https://registry.terraform.io/providers/hashicorp/tls/latest/docs) for terraform.

Check out [`certs.tf`](/terraform/certs.tf) for more information on how those certificates are generated.

In order to determine which "uses" each cert needed, I first used [falcoctl](https://github.com/falcosecurity/falcoctl) to generate TLS certificates, and then inspected them with `openssl`. Once I understood which fields each cert needed, I generated a CA and Certificate Signing Requests (CSRs) for both the server certificate and client certificate, and used terraform to perform the signing. The certificates' private keys are then accessible as terraform attributes to be used later with the Helm provider to deploy both falco and `falco-exporter`.

# Setting up Falco

Using the now-configured Helm provider for terraform, we can install Falco:

```
resource "helm_release" "falco" {
  name = "falco"

  repository = "https://falcosecurity.github.io/charts"
  chart = "falco"

  set {
    name = "falco.grpc.enabled"
    value = true
  }

  set {
    name = "falco.grpcOutput.enabled"
    value = true
  }

  # ... and then set some certificate values ...
  # see falco.tf for the details
}
```

# Setting up Prometheus & Grafana

Prometheus collects events, and Grafana is a pretty application with dashboards that can visualize those events.

Since these two applications are frequently deployed together as one big stack, there is a ["kube prometheus stack"](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack) Helm chart designed to do just that, and we will take advantage of that:

```
resource "helm_release" "prometheus-stack" {
  name = "prometheus-stack"

  repository = "https://prometheus-community.github.io/helm-charts"
  chart = "kube-prometheus-stack"

  set {
    name = "adminPassword"
    value = var.grafana_password
  }
}
```

# Configuring Falco to Export to Prometheus & Grafana
With Prometheus & Grafana now installed, all that's left is to tell falco to export its logs to Prometheus so they can be visualized in Grafana, and then set up the Falco dashboard in Grafana.

## falco-exporter

Setting up `falco-exporter` is yet another Helm release:

```
resource "helm_release" "falco_exporter" {
  name = "falco-exporter"

  repository = "https://falcosecurity.github.io/charts"
  chart = "falco-exporter"

  set {
    name = "serviceMonitor.enabled"
    value = true
  }

  set {
    name = "grafanaDashboard.enabled"
    value = true
  }

  # ... and then set some certificate values ...
  # see falco.tf for the details
}
```

`serviceMonitor.enabled` tells our Prometheus stack installation to monitor `falco-exporter` for events.

To test if `falco-exporter` is correctly exporting events to Prrometheus, I first set up a port forward to access the Prometheus web UI locally:

```
kubectl port-forward svc/prometheus-stack-kube-prom-prometheus 9090
```

Hitting `http://127.0.0.1:9090/targets` in a browser, I can see `falco-exporter` in the list:

![falco exporter target](/writeup/falco-exporter-target.png)

`grafanaDashboard.enabled` tells the `falco-exporter` chart to create the Falco dashboard in Grafana automatically.

To set up a port-forward to access Grafana:
```
kubectl port-forward svc/prometheus-stack-grafana 8080:80
```

Next, I head to `http://localhost:8080`, and login using the username `admin` and the password configured in the Helm release.

After logging in, the `falco-exporter` dashboard should be visible in the dashboards list, with nothing interesting in it, yet...

![empty falco dashboard](/writeup/empty-falco-dashboard.png)

# Generating Some Security Logs
In order to test the Falco installation, I'll generate a security event.

First, I grab the name of the first Falco pod:

```
❯ FIRST_FALCO_POD=$(kubectl get pod -l app=falco -o jsonpath="{.items[0].metadata.name}")
```

Then, I'll `exec` into the pod and just run a simple command (Falco reports on container `exec`s by default, so we'll see here whether it picks up an `exec` into itself!):

```
❯ kubectl exec -it $FIRST_FALCO_POD -- bash
root@falco-7p6wl:/# uname -a
Linux falco-7p6wl 4.19.0-17-cloud-amd64 #1 SMP Debian 4.19.194-3 (2021-07-18) x86_64 GNU/Linux
root@falco-7p6wl:/# exit
```

We can look through the Falco logs and see the spawned shell alert:

```
❯ kubectl logs $FIRST_FALCO_POD | grep shell
02:36:29.700730442: Notice A shell was spawned in a container with an attached terminal (user=root user_loginuid=-1 k8s.ns=default k8s.pod=falco-7p6wl container=8259a2086c95 shell=bash parent=runc cmdline=bash terminal=34816 container_id=8259a2086c95 image=docker.io/falcosecurity/falco) k8s.ns=default k8s.pod=falco-7p6wl container=8259a2086c95
```

We can also run the official falco event generator:
```
❯ kubectl apply -f https://raw.githubusercontent.com/falcosecurity/event-generator/master/deployment/role-rolebinding-serviceaccount.yaml
❯ kubectl apply -f https://raw.githubusercontent.com/falcosecurity/event-generator/master/deployment/run-as-job.yaml
```

![Falco event generator logs](/writeup/event-generator-logs.png)

The events show up in the Grafana dashboard:

![events showing up in Grafana dashboard](/writeup/events-in-dashboard.png)
