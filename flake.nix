{
  description = "digitalocean-falco";

  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    devShell.x86_64-linux = pkgs.mkShell {
      name = "digitalocean-falco";
      buildInputs = with pkgs; [
        kubectl
        doctl
        terraform_1
      ];
    };
  };
}
