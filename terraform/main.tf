terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.16.0"
    }
  }

  backend "http" {}
}

provider "digitalocean" {
  token = var.do_token
}

provider "helm" {
  kubernetes {
    host = digitalocean_kubernetes_cluster.falco_cluster.endpoint
    token = digitalocean_kubernetes_cluster.falco_cluster.kube_config[0].token
    cluster_ca_certificate = base64decode(
      digitalocean_kubernetes_cluster.falco_cluster.kube_config[0].cluster_ca_certificate
    )
  }
}
