resource "helm_release" "falco" {
  name = "falco"

  repository = "https://falcosecurity.github.io/charts"
  chart = "falco"

  set {
    name = "falco.grpc.enabled"
    value = true
  }

  set {
    name = "falco.grpcOutput.enabled"
    value = true
  }

  set {
    name = "certs.server.key"
    value = tls_private_key.falco_server.private_key_pem
  }

  set {
    name = "certs.server.crt"
    value = tls_locally_signed_cert.falco_server.cert_pem
  }

  set {
    name = "certs.ca.crt"
    value = tls_self_signed_cert.ca.cert_pem
  }
}

resource "helm_release" "prometheus-stack" {
  name = "prometheus-stack"

  repository = "https://prometheus-community.github.io/helm-charts"
  chart = "kube-prometheus-stack"

  set {
    name = "adminPassword"
    value = var.grafana_password
  }


  # The three rules that follow are required to get our prometheus instance to pick up the falco-exporter serviceMonitor without too much hassle..
  set {
    name = "prometheus.prometheusSpec.ruleSelectorNilUsesHelmValues"
    value = false
  }
  set {
    name = "prometheus.prometheusSpec.serviceMonitorSelectorNilUsesHelmValues"
    value = false
  }
  set {
    name = "prometheus.prometheusSpec.podMonitorSelectorNilUsesHelmValues"
    value = false
  }
}

resource "helm_release" "falco_exporter" {
  name = "falco-exporter"

  repository = "https://falcosecurity.github.io/charts"
  chart = "falco-exporter"

  set {
    name = "serviceMonitor.enabled"
    value = true
  }

  set {
    name = "grafanaDashboard.enabled"
    value = true
  }

  set {
    name = "certs.client.key"
    value = tls_private_key.exporter_client.private_key_pem
  }

  set {
    name = "certs.client.crt"
    value = tls_locally_signed_cert.exporter_client.cert_pem
  }

  set {
    name = "certs.ca.crt"
    value = tls_self_signed_cert.ca.cert_pem
  }
}
