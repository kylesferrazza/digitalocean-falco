resource "tls_private_key" "ca_key" {
  algorithm = "RSA"
}

resource "tls_self_signed_cert" "ca" {
  key_algorithm   = tls_private_key.ca_key.algorithm
  private_key_pem = tls_private_key.ca_key.private_key_pem

  # 730 hours == about a month
  validity_period_hours = 730

  # Generate a new certificate if Terraform is run within one
  # week of the certificate's expiration time.
  early_renewal_hours = 168

  allowed_uses = [
    "cert_signing",
    "key_encipherment",
    "digital_signature"
  ]

  is_ca_certificate = true

  subject {
    common_name  = "Falco Root CA"
    organization = "Falco"
  }
}

resource "tls_private_key" "falco_server" {
  algorithm = "RSA"
}

resource "tls_cert_request" "falco_server" {
  key_algorithm   = tls_private_key.falco_server.algorithm
  private_key_pem = tls_private_key.falco_server.private_key_pem

  dns_names = ["falco-grpc.default.svc.cluster.local"]

  subject {
    common_name  = "falco-grpc.default.svc.cluster.local"
    organization = "Falco"
  }
}

resource "tls_locally_signed_cert" "falco_server" {
  cert_request_pem = tls_cert_request.falco_server.cert_request_pem
  ca_key_algorithm = tls_private_key.ca_key.algorithm
  ca_private_key_pem = tls_private_key.ca_key.private_key_pem
  ca_cert_pem = tls_self_signed_cert.ca.cert_pem

  validity_period_hours = 730
  early_renewal_hours = 168

  allowed_uses = [
    "digital_signature",
    "key_encipherment",
    "server_auth",
  ]
}

resource "tls_private_key" "exporter_client" {
  algorithm = "RSA"
}

resource "tls_cert_request" "exporter_client" {
  key_algorithm   = tls_private_key.exporter_client.algorithm
  private_key_pem = tls_private_key.exporter_client.private_key_pem

  dns_names = ["falco-grpc.default.svc.cluster.local"]

  subject {
    common_name  = "falco-grpc.default.svc.cluster.local"
    organization = "Falco"
  }
}

resource "tls_locally_signed_cert" "exporter_client" {
  cert_request_pem = tls_cert_request.exporter_client.cert_request_pem
  ca_key_algorithm = tls_private_key.ca_key.algorithm
  ca_private_key_pem = tls_private_key.ca_key.private_key_pem
  ca_cert_pem = tls_self_signed_cert.ca.cert_pem

  validity_period_hours = 730
  early_renewal_hours = 168

  allowed_uses = [
    "digital_signature",
    "client_auth",
  ]
}
